<?php

namespace App\Http\Controllers;

use App\Http\Resources\CovidCollection;
use App\Models\Casos;
use Illuminate\Http\Request;
use App\Http\Resources\ShowResource;
use Illuminate\Support\Facades\DB;


class CasosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $casos = Casos::where('fecha',$id)->first();
        //$ia14 = DB::select(DB::raw("select * from ia7 where fecha ='$id'"));
        if (!$casos){
            return response()->json(['errors' =>Array(['code' => 404, 'message'=>'No existe la fecha'])],404);
        }
        //return response()->json(['status'=>'ok','data'=>$ia7],200);
        return new ShowResource($casos);
    }

    public function showAll()
    {

        $casos = Casos::all();
        if (! $casos)
        {
            return response()->json(['errors'=>Array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$casos],200);
    }

    public function store(Request $request)
    {
        $casos = new Casos();
        $casos->fecha = $request->fecha;
        $casos->ccaa_id = $request->ccaa_id;
        $casos->media = $request->media;
        $casos->save();
        return response()->json($casos);
    }

    public function showCollection($id,$id2)
    {
        if ($id>$id2){
            return response()->json(['errors' =>Array(['code' => 404, 'message'=>'Primera fecha superior a la segunda'])],404);
        }
        $casos = DB::select(DB::raw("SELECT * FROM casos WHERE fecha BETWEEN '$id' and '$id2'"));
        //$ia14 = DB::select(DB::raw("select * from ia14 where fecha ='$id'"));
        if (!$casos){
            return response()->json(['errors' =>Array(['code' => 404, 'message'=>'No existe la fecha'])],404);
        }
        //return response()->json(['status'=>'ok','data'=>$ia14],200);
        return new CovidCollection($casos);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
